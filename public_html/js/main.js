
var colors = ['#FF0000','#00FF00','#0000FF','#FFFF00', '#00FFFF','#FF00FF'];
/* Svg Environment */
var paths = new Array();
var pathOptions = new Array();
var polyline;


function initialize()
{
    /* Initialize pointer */
    $("#path-pointer").css("display", "block");
    $("#path-pointer").draggable({
        grid: [25,25], 
        cursor: "move",
        containment: "#svg-container"});

    /* Draw the initial table*/
    $("#table-container").drawTheTable([$("#spinner-2").val(), $("#spinner-1").val()]);

    /* Initialize SVG field */
    $('#svg-container').svg();
    
    
    $('#svg-container').mousedown(function(e){
        evt = e || window.event;        
        // make finger disappear
        $('#svg-container').hide(0);
        // get element at point of click
        starter = document.elementFromPoint(evt.clientX, evt.clientY);
        // send click to element at finger point
        $(starter).trigger(e);
        // bring back the finger
        $('#svg-container').show(0);
    });
    
    /* Initialize list of pathes and make it selectable */
    $( "#path-list" ).selectable({
        stop: function() {
            $('#svg-container').svg('get').clear(); 
            $( "#path-list .ui-selected:visible").each(function() {
                $('#svg-container').svg('get').polyline(paths[$("#path-list li").index(this)], pathOptions[$("#path-list li").index(this)]);
            });
        }
    });
    
    /* Initialize buttons */
    $("button").button();
    $("#hide-controls").click(function(){
        $("#controls").css("display", "none");
        $("#paths-controls-container").css("display", "none");
    });
    $("#clear-controls").click(function(){
        $("#pictures-list").empty();
    });

    /* Initilize pictures container */
    $("#pictures-list").droppable({
        accept: "img.cell-img",
        over:function(){  
            $(this).addClass('over');  
        },  
        out:function(){  
            $(this).removeClass('over');  
        }, 
        drop:function(event, ui){ 
            $(this).removeClass('over');
            if (ui.draggable.hasClass("inTable"))
                ui.draggable.parent().empty();
        } 
    });
    
    /* Check the browser compatibility */
    if(!window.FileReader)
        alert("Your browser doesn't support HTML 5 file reading technology. \
              You can still load images if they are stored in the same directory as the present html-file.");

    /* Initialize picture loader */
    $("#input-file").change(function(){
        for (var i = 0; i < this.files.length; i++) {
            if (this.files[i]){
                if(window.FileReader) { // firefox 3.6, Chrome 6, Webkit
                    var reader = new FileReader();
                    if(reader.addEventListener) {
                         reader.addEventListener('loadend', function (e) {
                            $("#pictures-list").append(createDroppableImage(e.target.result)); 
                        }, false);
                    } else {
                        reader.onloadend  = function (e) {
                            $("#pictures-list").append(createDroppableImage(e.target.result)); 
                        };                        
                    }
                    reader.readAsDataURL(this.files[i]);
                } else { //Safari
                    $("#pictures-list").append(createDroppableImage(this.files[i].name));
                }
            }
        }
    });
    
    $(".spinner").change(function( event, ui ) {
        $("#table-container").drawTheTable([$("#spinner-2").val(), $("#spinner-1").val()]);
    }
);

    $("#draw-path-button").click(function(){
        if ($(".list-temp").length > 0) finishPath();
        drawPath();
    });
    
    $("#deselect-paths-button").click(function(){
        $('#path-list .ui-selected').removeClass('ui-selected');
        $('#svg-container').svg('get').clear(); 
    });
    
    var $colorPicker = $("<div>Change color: </div>");
    for (var oneColor in colors){
        var $span = $("<span></span>");
        $span.click(function(){
            $('#svg-container').svg('get').clear(); 
            polyline = $('#svg-container').svg('get').polyline([], {});
            var color = $("circle", this).attr("fill");
            $( "#path-list li.ui-selected:visible").each(function() {
                var index = $( "#path-list li" ).index( this );
                pathOptions[index] = {fill: 'none', stroke: color, strokeWidth: 3};
                $('#svg-container').svg('get').polyline(paths[index], pathOptions[index]);
            });
            
        });
        $span.append($("<svg  xmlns='http://www.w3.org/2000/svg' version='1.1' width='12px' height='12px' style='cursor:pointer;' zoomAndPan='disable' >\n\
                        <circle cx='6' cy='6' r='5' stroke='grey' stroke-width='1' fill='"+colors[oneColor]+"'/></svg>"));
        $colorPicker.append($span);
    }
    $("#paths-controls-container").prepend($colorPicker);
    
    //    $("#the-over-net").css({
    //        top: $("#the-table").position().top , 
    //       left: $("#the-table").position().left + $("#table-container").outerWidth() / 2 - $("#the-table").outerWidth() / 2
    //    })
}

function drawPath(){
    var tempElement = $("<div></div>");
    tempElement.addClass("list-temp");
    tempElement.text("Path Nr." + $("#path-list li").length + " (click to finish)");
    $("#path-list").after(tempElement);
    $("#path-list").selectable( "disable" );
    
    tempElement.click(function (){ finishPath(); });
    
    var x0 = $("#svg-container").position().left + 1;
    var y0 = $("#svg-container").position().top + 1;
    var x = -1;
    var y = -1;
    paths[$("#path-list li").length] = new Array();
    pathOptions[$("#path-list li").length] = {fill: 'none', stroke: 'blue', strokeWidth: 3};
    polyline = $('#svg-container').svg('get').polyline([], pathOptions[$("#path-list li").length]);
    $("#path-pointer").on( "drag", function (event, ui){
                                      var newX = ui.position.left - x0;
                                      var newY = ui.position.top - y0;
                                      var index = $("#path-list li").length;
                                      if (x !== newX || y !== newY){
                                          paths[index].push([newX, newY]);
                                          if (polyline) $('#svg-container').svg('get').remove(polyline);
                                          polyline = $('#svg-container').svg('get').polyline(paths[index], pathOptions[index]);
                                          x = newX;
                                          y = newY;
                                      }
                                  });
}

function finishPath(){
    // List Element
    var listElement = $("<li></li>");
    listElement.addClass("ui-widget-content");
    listElement.addClass("ui-selected");
    $("#path-list").selectable( "enable" );
    listElement.text("Path Nr." + $("#path-list li").length + " ");
    $("#path-list").append(listElement);

    // Hide Button
    var $bttn = $("<button></button>");
    $bttn.button();
    $bttn.addClass("hide-bttn");
    $bttn.html($("<span>hide</span>").addClass("hide-text"));
    $bttn.click(function (){
        $(this).parents("li:first").hide();
        $('#svg-container').svg('get').clear(); 
        $("#path-list .ui-selected:visible").each(function() {
            var index = $( "#path-list li" ).index( this );
            $('#svg-container').svg('get').polyline(paths[index], pathOptions[index]);
        });
    });
    listElement.append($bttn);
    
    // Remove temporary element
    $(".list-temp").remove();
    $("#path-pointer").unbind( "drag");
}


function createDroppableImage(source){
    var $img = $("<img src=\"#\">")
            .addClass("cell-img")
            .addClass("inDock")
            .draggable({revert: "invalid",
                        cursor: "move",
                        helper: "clone",
                        zIndex: 100});
    $img.attr('src', source);
    return $img;
}

(function( $ ) {
    $.fn.drawTheTable = function(size){
        this.width(size[1] * 110);
        var $table = $("#the-table", this);
        if ($table.length === 0) {
            $table = $("<table></table>");
            this.append($table);
        }
        $table.attr("id", "the-table");
        $table.empty();
        for(var i = 0; i < size[0]; i++){
            var row = $("<tr></tr>");
            for(var j = 0; j < size[1]; j++){
                var cell = $("<td class=\"cell-element\"></td>");
                row.append(cell);
            }
            $table.append(row);
        }
        
        $table.position({
            of: this,
            my: "left top",
            at: "left top"
        });
        
        $('#svg-container').css({
            width: $("#the-table").width()+1,
            height: $("#the-table").height()
        });        
        $("#svg-container").position({
            of: this,
            my: "left top",
            at: "left top"
        });
        $('#svg-container svg').width($("#svg-container").width());
        $('#svg-container svg').height($("#svg-container").height());
        
        $("#path-pointer").position({
            of: this,
            my: "left top",
            at: "left+2 top+2"
        });

        
        $("td.cell-element").droppable({
            accept: "img.cell-img",
            over:function(){  
                $(this).addClass('over');  
            },  
            out:function(){  
                $(this).removeClass('over');  
            }, 
            drop:function(event, ui){ 
                if (ui.draggable.hasClass("inDock")){
                    var $c = ui.draggable.clone();
                    $c.removeClass("inDock")
                            .addClass("inTable")
                            .draggable({revert: "invalid", 
                                        cursor: "move", 
                                        stack: "img.cell-img"})
                            .css({top:0,left:0});
                    $(this).empty().append($c);
                } else {
                    $(this).removeClass('over');
                    ui.draggable.parent().append($("img:first", this));
                    ui.draggable.css({top:0,left:0});
                    $(this).append(ui.draggable);
                }
            } 
        });
    };
})(jQuery);